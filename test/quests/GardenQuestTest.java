package quests;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import board.Board;
import board.Coordinates;
import board.Tile;
import enums.Improvement;
import enums.TileColor;
class GardenQuestTest {
	@Test
	void testResolveQuest_GivenTriangleShapeThatIsFulfilledOnBoard_ReturnsTrue() {
		Board b = new Board();
		Tile green = new Tile(TileColor.GREEN,Improvement.IRRIGATED);
		b.addTile(new Coordinates(1,0),green);
		b.addTile(new Coordinates(1,1), green);
		b.addTile(new Coordinates(2,1), green);
		Quest quest = new GardenQuest(GardenShape.TRIANGLE,TileColor.GREEN,5);
		Assert.assertTrue(quest.resolved(b, null));
	}
	@Test
	void testResolveQuest_GivenTriangleShapeThatIsNotWateredOnBoard_ReturnsFalse() {
		Board b = new Board();
		Tile green = new Tile(TileColor.GREEN,Improvement.NONE);
		b.addTile(new Coordinates(1,0),green);
		b.addTile(new Coordinates(1,1), green);
		b.addTile(new Coordinates(2,1), green);
		System.out.println(b.get(new Coordinates(1,0)).isWatered());
		System.out.println(b.get(new Coordinates(1,1)).isWatered());
		System.out.println(b.get(new Coordinates(2,1)).isWatered());
		Quest quest = new GardenQuest(GardenShape.TRIANGLE,TileColor.GREEN,5);
		Assert.assertFalse(quest.resolved(b, null));
	}
	@Test
	void testResolveQuest_GivenTriangleShapeThatIsNotFulfilledOnBoardWithSameColor_ReturnsFalse() {
		Board b = new Board();
		Tile green = new Tile(TileColor.GREEN,Improvement.IRRIGATED);
		Tile pink = new Tile(TileColor.PINK,Improvement.IRRIGATED);
		b.addTile(new Coordinates(1,0),green);
		b.addTile(new Coordinates(1,1), green);
		b.addTile(new Coordinates(2,1), pink);
		System.out.println(b.get(new Coordinates(1,0)).isWatered());
		System.out.println(b.get(new Coordinates(1,1)).isWatered());
		System.out.println(b.get(new Coordinates(2,1)).isWatered());
		Quest quest = new GardenQuest(GardenShape.TRIANGLE,TileColor.GREEN,5);
		Assert.assertFalse(quest.resolved(b, null));
	}
	@Test
	void testResolveQuest_GivenCrescentShapeThatIsFulfilledOnBoard_ReturnsTrue() {
		Board b = new Board();
		Tile yellow = new Tile(TileColor.YELLOW,Improvement.IRRIGATED);
		b.addTile(new Coordinates(1,0),yellow);
		b.addTile(new Coordinates(1,1), yellow);
		b.addTile(new Coordinates(2,1), yellow);
		b.addTile(new Coordinates(2,2), yellow);
		Quest quest = new GardenQuest(GardenShape.CRESCENT,TileColor.YELLOW,5);
		Assert.assertTrue(quest.resolved(b, null));
	}
	@Test
	void testResolveQuest_GivenCrescentShapeThatIsNotWateredOnBoard_ReturnsFalse() {
		Board b = new Board();
		Tile yellow = new Tile(TileColor.YELLOW,Improvement.NONE);
		b.addTile(new Coordinates(1,0),yellow);
		b.addTile(new Coordinates(1,1), yellow);
		b.addTile(new Coordinates(2,1), yellow);
		b.addTile(new Coordinates(2,2), yellow);
		Quest quest = new GardenQuest(GardenShape.CRESCENT,TileColor.YELLOW,5);
		Assert.assertFalse(quest.resolved(b, null));
	}
	@Test
	void testResolveQuest_GivenCrescentShapeThatIsNotFulfilledWithSameColorOnBoard_ReturnsFalse() {
		Board b = new Board();
		Tile green = new Tile(TileColor.GREEN,Improvement.IRRIGATED);
		Tile yellow = new Tile(TileColor.YELLOW,Improvement.IRRIGATED);
		b.addTile(new Coordinates(1,0),yellow);
		b.addTile(new Coordinates(1,1), yellow);
		b.addTile(new Coordinates(2,1), green);
		b.addTile(new Coordinates(2,2), green);
		Quest quest = new GardenQuest(GardenShape.CRESCENT,TileColor.YELLOW,5);
		Assert.assertFalse(quest.resolved(b, null));
	}
	//
	@Test
	void testResolveQuest_GivenLineShapeThatIsFulfillOnBoard_ReturnsTrue() {
		Board b = new Board();
		Tile yellow = new Tile(TileColor.YELLOW,Improvement.IRRIGATED);
		b.addTile(new Coordinates(1,0),yellow);
		b.addTile(new Coordinates(1,1), yellow);
		b.addTile(new Coordinates(2,1), yellow);
		b.addTile(new Coordinates(2,2), yellow);
		b.addTile(new Coordinates(3,1), yellow);
		b.addTile(new Coordinates(3,2), yellow);
		Quest quest = new GardenQuest(GardenShape.LINE,TileColor.YELLOW,5);
		Assert.assertTrue(quest.resolved(b, null));
	}
	@Test
	void testResolveQuest_GivenLineShapeThatIsNotWateredOnBoard_ReturnsTrue() {
		Board b = new Board();
		Tile yellow = new Tile(TileColor.YELLOW,Improvement.NONE);
		b.addTile(new Coordinates(1,0),yellow);
		b.addTile(new Coordinates(1,1), yellow);
		b.addTile(new Coordinates(2,1), yellow);
		b.addTile(new Coordinates(2,2), yellow);
		b.addTile(new Coordinates(3,1), yellow);
		b.addTile(new Coordinates(3,2), yellow);
		Quest quest = new GardenQuest(GardenShape.LINE,TileColor.YELLOW,5);
		Assert.assertFalse(quest.resolved(b, null));
	}
	@Test
	void testResolveQuest_GivenLineShapeThatIsNotFulfilledWithSameColorOnBoard_ReturnsFalse() {
		Board b = new Board();
		Tile green = new Tile(TileColor.GREEN,Improvement.IRRIGATED);
		Tile yellow = new Tile(TileColor.YELLOW,Improvement.IRRIGATED);
		b.addTile(new Coordinates(1,0),yellow);
		b.addTile(new Coordinates(1,1), yellow);
		b.addTile(new Coordinates(2,1), green);
		b.addTile(new Coordinates(2,2), green);
		b.addTile(new Coordinates(2,1), yellow);
		b.addTile(new Coordinates(2,2), green);
		Quest quest = new GardenQuest(GardenShape.LINE,TileColor.YELLOW,5);
		Assert.assertFalse(quest.resolved(b, null));
	}
	@Test
	void testResolveQuest_GivenDiamondShapeThatIsFulfilledOnBoard_ReturnsTrue() {
		Board b = new Board();
		Tile yellow = new Tile(TileColor.YELLOW,Improvement.IRRIGATED);
		b.addTile(new Coordinates(1,0),yellow);
		b.addTile(new Coordinates(1,1), yellow);
		b.addTile(new Coordinates(2,1), yellow);
		b.addTile(new Coordinates(2,2), yellow);
		b.addTile(new Coordinates(2,3), yellow);
		Quest quest = new GardenQuest(GardenShape.DIAMOND,TileColor.YELLOW,5);
		Assert.assertTrue(quest.resolved(b, null));
	}
	@Test
	void testResolveQuest_GivenDiamondShapeThatIsNotWateredOnBoard_ReturnsTrue() {
		Board b = new Board();
		Tile yellow = new Tile(TileColor.YELLOW,Improvement.NONE);
		b.addTile(new Coordinates(1,0),yellow);
		b.addTile(new Coordinates(1,1), yellow);
		b.addTile(new Coordinates(2,1), yellow);
		b.addTile(new Coordinates(2,2), yellow);
		Quest quest = new GardenQuest(GardenShape.DIAMOND,TileColor.YELLOW,5);
		Assert.assertFalse(quest.resolved(b, null));
	}
	@Test
	void testResolveQuest_GivenDiamondShapeThatIsNotFulfilledWithSameColorOnBoard_ReturnsFalse() {
		Board b = new Board();
		Tile green = new Tile(TileColor.GREEN,Improvement.IRRIGATED);
		Tile yellow = new Tile(TileColor.YELLOW,Improvement.IRRIGATED);
		b.addTile(new Coordinates(1,0),yellow);
		b.addTile(new Coordinates(1,1), yellow);
		b.addTile(new Coordinates(2,1), green);
		b.addTile(new Coordinates(2,2), green);
		Quest quest = new GardenQuest(GardenShape.DIAMOND,TileColor.YELLOW,5);
		Assert.assertFalse(quest.resolved(b, null));
	}
	@Test
	void testResolveQuest_GivenDiamondShapeWithSecondaryColorThatIsFulfilledOnBoard_ReturnsTrue() {
		Board b = new Board();
		Tile green = new Tile(TileColor.GREEN,Improvement.IRRIGATED);
		Tile yellow = new Tile(TileColor.YELLOW,Improvement.IRRIGATED);
		b.addTile(new Coordinates(1,0),yellow);
		b.addTile(new Coordinates(1,1), yellow);
		b.addTile(new Coordinates(2,1), green);
		b.addTile(new Coordinates(2,2), yellow);
		b.addTile(new Coordinates(2,3), green);
		Quest quest = new GardenQuest(GardenShape.DIAMOND,TileColor.YELLOW,TileColor.GREEN,5);
		Assert.assertTrue(quest.resolved(b, null));
	}
}
