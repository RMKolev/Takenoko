package quests;

import java.util.ArrayList;
import java.util.Iterator;

import board.Board;
import board.Coordinates;
import board.Tile;
import enums.TileColor;

public class GardenQuest extends Quest {
	private GardenShape shape;
	private TileColor color;
	private TileColor secondaryColor;

	public GardenQuest() {
		super();
		this.shape = GardenShape.TRIANGLE;
		this.color = TileColor.GREEN;
		this.secondaryColor = this.color;

	}

	public GardenQuest(GardenShape shape, TileColor color, int points) {
		super(points);
		this.color = color;
		this.shape = shape;
		this.secondaryColor = this.color;
	}

	public GardenQuest(GardenShape shape, TileColor color, TileColor secondaryColor, int points) {
		super(points);
		this.color = color;
		this.shape = shape;
		this.secondaryColor = secondaryColor;

	}

	@Override
	boolean resolved(Board b, int[] bamboo) {
		return resolveQuest(b);
	}

	private boolean resolveQuest(Board b) {
		ArrayList<Coordinates> search = b.getTilesByColor(this.color);
		Iterator<Coordinates> it = search.iterator();
		switch (this.shape) {
		case TRIANGLE:
			return resolve(b, it, 1);
		case LINE:
			return resolve(b, it, 3);
		case CRESCENT:
			return resolve(b, it, 2);
		case DIAMOND:
			return resolveDiamond(b,it);
		default:
			break;
		}
		return false;
	}

	private boolean resolve(Board board, Iterator<Coordinates> iterator, int secondNeighborDifference) {
		while (iterator.hasNext()) {
			Coordinates currentCoordinates = iterator.next();
			for (int i = 0; i < 6; i++) {
				Tile current = board.get(currentCoordinates);
				Tile neighbor1 = board.get(currentCoordinates.getNeighbor(i));
				Tile neighbor2 = board.get(currentCoordinates.getNeighbor(i + secondNeighborDifference));
				if (neighbor1 != null && neighbor2 != null) {
					if ((current.isWatered() && neighbor1.isWatered() && neighbor2.isWatered())
							&& neighbor1.getColor() == neighbor2.getColor()
							&& neighbor2.getColor() == current.getColor()) {
						return true;
					}
				}
			}
		}
		return false;
	}

	private boolean resolveDiamond(Board board, Iterator<Coordinates> iterator) {
		while (iterator.hasNext()) {
			Coordinates currentCoordinates = iterator.next();
			for (int i = 0; i < 6; i++) {
				Tile current = board.get(currentCoordinates);
				Tile neighbor1 = board.get(currentCoordinates.getNeighbor(i));
				Tile neighbor2 = board.get(currentCoordinates.getNeighbor(i + 1));
				Tile neighbor3 = board.get(currentCoordinates.getNeighbor(i + 2));
				if (neighbor1 != null && neighbor2 != null && neighbor3 != null) {
					if ((current.isWatered() && neighbor1.isWatered() && neighbor2.isWatered() && neighbor3.isWatered())
							&& current.getColor() == neighbor2.getColor()
							&& (neighbor1.getColor() == neighbor3.getColor()&& neighbor3.getColor()==this.secondaryColor)) {
						return true;
					}
				}
			}
		}
		return false;
	}

}
