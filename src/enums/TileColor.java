package enums;

public enum TileColor {
    GREEN,
    YELLOW,
    PINK,
    POND
}
