package board;

import enums.Improvement;
import enums.TileColor;

public class Tile {
	boolean[] pipes = new boolean[6];
	byte bambooLevel = 0;
	TileColor color;
	Improvement mod;

	public Tile() {
		this.pipes = new boolean[6];
		this.color = TileColor.GREEN;
		this.bambooLevel = 0;
		this.mod = Improvement.NONE;
	}
	public Tile(final Tile toCopy) {
		this.pipes = new boolean[6];
		for(int i=0;i<6;i++) {
			this.pipes[i] = toCopy.pipes[i];
		}
		this.color = toCopy.color;
		this.bambooLevel = toCopy.bambooLevel;
		this.mod = toCopy.mod;
	}
	void makePond() {
		this.pipes = new boolean[6];
		for (int i = 0; i < 6; i++) {
			this.pipes[i] = true;
		}
		this.color = TileColor.POND;
		this.mod = Improvement.IRRIGATED;
	}

	public Tile(TileColor color, Improvement mod) {
		this.mod = mod;
		this.color = color;
		this.bambooLevel = 0;
	}

	public boolean isWatered() {
		if (this.mod == Improvement.IRRIGATED) {
			return true;
		}
		for (int i = 0; i < 6; i++) {
			if (this.pipes[i] == true) {
				return true;
			}
		}
		return false;
	}

	public boolean[] getPipes() {
		return this.pipes;
	}

	public byte getBambooLevel() {
		return this.bambooLevel;
	}

	@Override
	public String toString() {
		if(this.color == TileColor.POND) {
			return "Tile [POND]";
		}
		return "Tile [bambooLevel=" + bambooLevel + ", mod=" + mod + ", color=" + color + " watered="+this.isWatered()+"]";
	}

	public void growBamboo() {
		if (this.color != TileColor.POND) {
			if (this.isWatered()) {
				if (this.bambooLevel < 4) {
					this.bambooLevel+=this.mod==Improvement.FERTILISED?2:1;
				}
				if(this.bambooLevel > 4) {
				    this.bambooLevel = 4;
				}
			}
		}
	}

	public void removeBamboo() {
		if (this.bambooLevel > 0) {
			this.bambooLevel--;
		}
	}

	public Improvement getMod() {
		return this.mod;
	}

	public void addMod(Improvement newMod) {
		if (this.mod == Improvement.NONE) {
			this.mod = newMod;
		}
	}

	public TileColor getColor() {
		return this.color;
	}

	public boolean hasPipe(int n) {
		n = n % 6;
		return this.pipes[n];
	}

	public void addPipe(int n) {
		n = n % 6;
		if (!this.pipes[n]) {
			this.pipes[n] = true;
		}
	}
}
