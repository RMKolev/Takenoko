package quests;

import board.Board;

public abstract class Quest {
	private int points = 0;

	public int getPoints() {
		return this.points;
	}

	public Quest() {
		this.points = 2;
	}

	public Quest(int points) {
		this.points = points;
	}

	abstract boolean resolved(Board board, int[] bamboo);

}
