package player;

import quests.Quest;

import enums.Improvement;
import enums.TileColor;

public class Player {

    String name;
    Quest[] quests;
    int points;
    int completedQuests = 0;
    int[] bamboo;
    int pipes;
    int[] improvements;

    public Player(String name,Quest[] quests) {
        this.name = name;
        this.quests = quests;
        this.points = 0;
        this.bamboo = new int[3];
        this.pipes = 0;
        this.improvements = new int[3];
    }

    public int getImprovement(Improvement improvement) {
        if (improvement != Improvement.NONE) {
            return this.improvements[improvement.ordinal() - 1];
        } else {
            System.out.println("Players have no such improvement.");
            return -1;
        }
    }

    public int getBamboo(TileColor color) {
        if (color != TileColor.POND) {
            return this.bamboo[color.ordinal()];
        } else {
            System.out.println("Players have no such color bamboo.");
            return -1;
        }
    }

    public int getPipeCount() {
        return this.pipes;
    }

    void addBamboo(TileColor color) {
        if (color != TileColor.POND) {
            this.bamboo[color.ordinal()]++;
        } else {
            System.out.println("Players have no such color bamboo.");
        }
    }

    void addImprovement(Improvement improvement) {
        if (improvement != Improvement.NONE) {
            this.improvements[improvement.ordinal() - 1]++;
        } else {
            System.out.println("Players have no such improvement.");
        }
    }

    void addPipe() {
        this.pipes++;
    }

    void removeBamboo(TileColor color) {
        if (color != TileColor.POND) {
            if (this.bamboo[color.ordinal()] > 0) {
                this.bamboo[color.ordinal()]--;
            }
        } else {
            System.out.println("Players have no such color bamboo.");
        }
    }

    void removeImprovement(Improvement improvement) {
        if (improvement != Improvement.NONE) {
            if (this.improvements[improvement.ordinal() - 1] > 0) {
                this.improvements[improvement.ordinal() - 1]--;
            }
        } else {
            System.out.println("Players have no such improvement.");
        }
    }

    void removePipe() {
        if (this.pipes > 0) {
            this.pipes--;
        }
    }

}
