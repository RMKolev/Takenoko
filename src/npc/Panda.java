package npc;

import board.Board;
import board.Coordinates;
import enums.Improvement;
import player.Player;

public class Panda {
    Coordinates position;

    public Panda() {
        this.position = new Coordinates();
    }

    void move(int direction, int steps, Board board, Player player) {
        Coordinates previousPosition = this.position;
        for (int i = 0; i < steps; i++) {
            if (board.get(this.position.getNeighbor(direction)) != null) {
                this.position = this.position.getNeighbor(direction);
            } else {
                this.position = previousPosition;
                return;
            }
        }
        if (board.get(this.position).getMod() != Improvement.PROTECTED) {
            board.get(this.position).removeBamboo();
        }
    }

}
