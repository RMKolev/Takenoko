package board;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

class CoordinatesTest {

	@Test
	void testIsValid_GivenNegativeIndex_ReturnsFalse() {
		Coordinates test = new Coordinates(0, -5);
		Assert.assertFalse(test.isValid());
	}

	@Test
	void testIsValid_GivenNegativeLevel_ReturnsFalse() {
		Coordinates test = new Coordinates(-1, 5);
		Assert.assertFalse(test.isValid());
	}

	@Test
	void testIsValid_GivenBiggerThanAllowedIndex_ReturnsFalse() {
		Coordinates test = new Coordinates(2, 15);
		Assert.assertFalse(test.isValid());
	}

	@Test
	void testIsValid_GivenCorrectIndexAndNonNegativeLevel_ReturnsTrue() {
		Coordinates test = new Coordinates(1, 5);
		Assert.assertTrue(test.isValid());
	}

	@Test
	void testIsStraight_GivenZeroCoordinates_ReturnsTrue() {
		Coordinates test = new Coordinates(0, 0);
		Assert.assertTrue(test.isStraight());
	}

	@Test
	void testIsStraight_GivenStraightCoordinates_ReturnsTrue() {
		Coordinates test = new Coordinates(2, 6);
		Assert.assertTrue(test.isStraight());
	}

	@Test
	void testIsStraight_GivenNonStraightCoordinates_ReturnsTrue() {
		Coordinates test = new Coordinates(3, 14);
		Assert.assertFalse(test.isStraight());
	}

	@Test
	void testGetNeighbor_GivenZeroCoordinate_ReturnsValidNeighbors() {
		Coordinates test = new Coordinates(0, 0);
		for (int i = 0; i < 6; i++) {
			Assert.assertEquals(new Coordinates(1, i), test.getNeighbor(i));
		}
	}

	@Test
	void testGetNeighbor_GivenStraightCoordinatesFromFirstSextant_ReturnsValidNeighbors() {
		Coordinates test = new Coordinates(2, 0);
		Coordinates[] result = new Coordinates[6];
		result[0] = new Coordinates(3, 0);
		result[1] = new Coordinates(3, 1);
		result[2] = new Coordinates(2, 1);
		result[3] = new Coordinates(1, 0);
		result[4] = new Coordinates(2, 11);
		result[5] = new Coordinates(3, 17);

		for (int i = 0; i < 6; i++) {
			Assert.assertEquals(result[i], test.getNeighbor(i));
		}
	}

	@Test
	void testGetNeighbor_GivenNonStraightCoordinatesFromFirstSextant_ReturnsValidNeighbors() {
		Coordinates test = new Coordinates(2, 1);
		Coordinates[] result = new Coordinates[6];
		result[0] = new Coordinates(3, 1);
		result[1] = new Coordinates(3, 2);
		result[2] = new Coordinates(2, 2);
		result[3] = new Coordinates(1, 1);
		result[4] = new Coordinates(1, 0);
		result[5] = new Coordinates(2, 0);

		for (int i = 0; i < 6; i++) {
			Assert.assertEquals(result[i], test.getNeighbor(i));
		}
	}

	@Test
	void testGetNeighbor_GivenStraightCoordinatesFromSecondSextant_ReturnsValidNeighbors() {
		Coordinates test = new Coordinates(3, 3);
		Coordinates[] result = new Coordinates[6];
		result[0] = new Coordinates(4, 3);
		result[1] = new Coordinates(4, 4);
		result[2] = new Coordinates(4, 5);
		result[3] = new Coordinates(3, 4);
		result[4] = new Coordinates(2, 2);
		result[5] = new Coordinates(3, 2);

		for (int i = 0; i < 6; i++) {
			Assert.assertEquals(result[i], test.getNeighbor(i));
		}
	}

	@Test
	void testGetNeighbor_GivenNonStraightCoordinatesFromSecondSextant_ReturnsValidNeighbors() {
		Coordinates test = new Coordinates(3, 4);
		Coordinates[] result = new Coordinates[6];
		result[0] = new Coordinates(3, 3);
		result[1] = new Coordinates(4, 5);
		result[2] = new Coordinates(4, 6);
		result[3] = new Coordinates(3, 5);
		result[4] = new Coordinates(2, 3);
		result[5] = new Coordinates(2, 2);

		for (int i = 0; i < 6; i++) {
			Assert.assertEquals(result[i], test.getNeighbor(i));
		}
	}

	@Test
	void testGetNeighbor_GivenStraightCoordinatesFromThirdSextant_ReturnsValidNeighbors() {
		Coordinates test = new Coordinates(3, 6);
		Coordinates[] result = new Coordinates[6];
		result[0] = new Coordinates(3, 5);
		result[1] = new Coordinates(4, 7);
		result[2] = new Coordinates(4, 8);
		result[3] = new Coordinates(4, 9);
		result[4] = new Coordinates(3, 7);
		result[5] = new Coordinates(2, 4);

		for (int i = 0; i < 6; i++) {
			Assert.assertEquals(result[i], test.getNeighbor(i));
		}
	}

	@Test
	void testGetNeighbor_GivenNonStraightCoordinatesFromThirdSextant_ReturnsValidNeighbors() {
		Coordinates test = new Coordinates(3, 7);
		Coordinates[] result = new Coordinates[6];
		result[0] = new Coordinates(2, 4);
		result[1] = new Coordinates(3, 6);
		result[2] = new Coordinates(4, 9);
		result[3] = new Coordinates(4, 10);
		result[4] = new Coordinates(3, 8);
		result[5] = new Coordinates(2, 5);

		for (int i = 0; i < 6; i++) {
			Assert.assertEquals(result[i], test.getNeighbor(i));
		}
	}

	@Test
	void testGetNeighbor_GivenStraightCoordinatesFromFourthSextant_ReturnsValidNeighbors() {
		Coordinates test = new Coordinates(3, 9);
		Coordinates[] result = new Coordinates[6];
		result[0] = new Coordinates(2, 6);
		result[1] = new Coordinates(3, 8);
		result[2] = new Coordinates(4, 11);
		result[3] = new Coordinates(4, 12);
		result[4] = new Coordinates(4, 13);
		result[5] = new Coordinates(3, 10);

		for (int i = 0; i < 6; i++) {
			Assert.assertEquals(result[i], test.getNeighbor(i));
		}
	}

	@Test
	void testGetNeighbor_GivenNonStraightCoordinatesFromFourthSextant_ReturnsValidNeighbors() {
		Coordinates test = new Coordinates(3, 11);
		Coordinates[] result = new Coordinates[6];
		result[0] = new Coordinates(2, 8);
		result[1] = new Coordinates(2, 7);
		result[2] = new Coordinates(3, 10);
		result[3] = new Coordinates(4, 14);
		result[4] = new Coordinates(4, 15);
		result[5] = new Coordinates(3, 12);

		for (int i = 0; i < 6; i++) {
			Assert.assertEquals(result[i], test.getNeighbor(i));
		}
	}

	@Test
	void testGetNeighbor_GivenStraightCoordinatesFromFifthSextant_ReturnsValidNeighbors() {
		Coordinates test = new Coordinates(3, 12);
		Coordinates[] result = new Coordinates[6];
		result[0] = new Coordinates(3, 13);
		result[1] = new Coordinates(2, 8);
		result[2] = new Coordinates(3, 11);
		result[3] = new Coordinates(4, 15);
		result[4] = new Coordinates(4, 16);
		result[5] = new Coordinates(4, 17);

		for (int i = 0; i < 6; i++) {
			Assert.assertEquals(result[i], test.getNeighbor(i));
		}
	}

	@Test
	void testGetNeighbor_GivenNonStraightCoordinatesFromFifthSextant_ReturnsValidNeighbors() {
		Coordinates test = new Coordinates(3, 13);
		Coordinates[] result = new Coordinates[6];
		result[0] = new Coordinates(3, 14);
		result[1] = new Coordinates(2, 9);
		result[2] = new Coordinates(2, 8);
		result[3] = new Coordinates(3, 12);
		result[4] = new Coordinates(4, 17);
		result[5] = new Coordinates(4, 18);

		for (int i = 0; i < 6; i++) {
			Assert.assertEquals(result[i], test.getNeighbor(i));
		}
	}

	@Test
	void testGetNeighbor_GivenStraightCoordinatesFromSixthSextant_ReturnsValidNeighbors() {
		Coordinates test = new Coordinates(3, 15);
		Coordinates[] result = new Coordinates[6];
		result[0] = new Coordinates(4, 21);
		result[1] = new Coordinates(3, 16);
		result[2] = new Coordinates(2, 10);
		result[3] = new Coordinates(3, 14);
		result[4] = new Coordinates(4, 19);
		result[5] = new Coordinates(4, 20);

		for (int i = 0; i < 6; i++) {
			Assert.assertEquals(result[i], test.getNeighbor(i));
		}
	}

	@Test
	void testGetNeighbor_GivenNonStraightCoordinatesFromSixstSextant_ReturnsValidNeighbors() {
		Coordinates test = new Coordinates(3, 17);
		Coordinates[] result = new Coordinates[6];
		result[0] = new Coordinates(4, 23);
		result[1] = new Coordinates(3, 0);
		result[2] = new Coordinates(2, 0);
		result[3] = new Coordinates(2, 11);
		result[4] = new Coordinates(3, 16);
		result[5] = new Coordinates(4, 22);

		for (int i = 0; i < 6; i++) {
			Assert.assertEquals(result[i], test.getNeighbor(i));
		}
	}
}
