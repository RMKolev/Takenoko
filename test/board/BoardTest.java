package board;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import enums.Improvement;
import enums.TileColor;

class BoardTest {

    @Test
    void testAddTile_GivenFirstTileWhichIsValid_ThenArrayContainsGivenTile() {
        Board test = new Board();
        Tile temp = new Tile();
        test.addTile(new Coordinates(1, 0), temp);

        Assert.assertTrue(test.get(new Coordinates(1, 0)) != null);
    }

    @Test
    void testAddTile_GivenFirstTileWhichIsNotValid_ThenArrayDoesntContainsGivenTile() {
        Board test = new Board();
        Tile temp = new Tile();
        test.addTile(new Coordinates(3, 0), temp);

        Assert.assertFalse(test.get(new Coordinates(3, 0)) != null);
    }

    @Test
    void testAddTile_GivenNotFirstTileWhichIsNotValid_ThenArrayDoesntContainsGivenTile() {
        Board test = new Board();
        Tile temp = new Tile();
        test.addTile(new Coordinates(1, 0), temp);
        temp = new Tile();
        test.addTile(new Coordinates(2, 11), temp);

        Assert.assertFalse(test.get(new Coordinates(2, 11)) != null);
    }

    @Test
    void testAddTile_GivenNotFirstTileWhichIsValid_ThenArrayContainsGivenTile() {
        Board test = new Board();
        Tile temp = new Tile();
        test.addTile(new Coordinates(1, 0), temp);
        temp = new Tile();
        test.addTile(new Coordinates(1, 5), temp);
        temp = new Tile();
        test.addTile(new Coordinates(2, 11), temp);

        Assert.assertTrue(test.get(new Coordinates(2, 11)) != null);
    }

    @Test
    void testAddTile_GivenFirstTileWhichIsValid_ThenBambooLevelBecomesOne() {
        Board test = new Board();
        Tile temp = new Tile();
        test.addTile(new Coordinates(1, 0), temp);

        Assert.assertTrue(test.get(new Coordinates(1, 0)).getBambooLevel() == 1);
    }

    @Test
    void testAddTile_GivenTileWhichIsValidAndNotIrrigated_ThenBambooLevelStaysZero() {
        Board test = new Board();
        Tile temp = new Tile();
        test.addTile(new Coordinates(1, 0), temp);
        temp = new Tile();
        test.addTile(new Coordinates(1, 5), temp);
        temp = new Tile();
        test.addTile(new Coordinates(2, 11), temp);

        Assert.assertTrue(test.get(new Coordinates(2, 11)).getBambooLevel() == 0);
    }

    @Test
    void testAddPipe_GivenFirstPipeWhichIsValid_ThenBothTilesContainPipe() {
        Board test = new Board();
        Tile temp = new Tile();
        test.addTile(new Coordinates(1, 0), temp);
        temp = new Tile();
        test.addTile(new Coordinates(1, 5), temp);
        temp = new Tile();
        test.addTile(new Coordinates(2, 11), temp);
        temp = new Tile();
        test.addTile(new Coordinates(2, 10), temp);
        test.addPipe(new Coordinates(1, 0), 4);

        Assert.assertTrue(test.get(new Coordinates(1, 0)).hasPipe(4));
        Assert.assertTrue(test.get(new Coordinates(1, 5)).hasPipe(1));
    }

    @Test
    void testAddPipe_GivenPipeWhichIsValid_ThenBothTilesContainPipe() {
        Board test = new Board();
        Tile temp = new Tile();
        test.addTile(new Coordinates(1, 0), temp);
        temp = new Tile();
        test.addTile(new Coordinates(1, 5), temp);
        temp = new Tile();
        test.addTile(new Coordinates(2, 11), temp);
        temp = new Tile();
        test.addTile(new Coordinates(2, 10), temp);
        test.addPipe(new Coordinates(1, 0), 4);
        test.addPipe(new Coordinates(2, 11), 3);
        test.addPipe(new Coordinates(2, 10), 1);

        Assert.assertTrue(test.get(new Coordinates(2, 10)).hasPipe(1));
        Assert.assertTrue(test.get(new Coordinates(2, 11)).hasPipe(4));
    }

    @Test
    void testAddPipe_GivenPipeWhichIsNotConnectedWithOtherPipe_ThenBothTilesDontContainPipe() {
        Board test = new Board();
        Tile temp = new Tile();
        test.addTile(new Coordinates(1, 0), temp);
        temp = new Tile();
        test.addTile(new Coordinates(1, 5), temp);
        temp = new Tile();
        test.addTile(new Coordinates(2, 11), temp);
        temp = new Tile();
        test.addTile(new Coordinates(2, 10), temp);
        test.addPipe(new Coordinates(1, 0), 4);
        test.addPipe(new Coordinates(2, 10), 1);

        Assert.assertFalse(test.get(new Coordinates(2, 10)).hasPipe(1));
        Assert.assertFalse(test.get(new Coordinates(2, 11)).hasPipe(4));
    }

    @Test
    void testAddPipe_GivenPipeWhichIsNotBetweenTwoTiles_ThenTileDoesntContainPipe() {
        Board test = new Board();
        Tile temp = new Tile();
        test.addTile(new Coordinates(1, 0), temp);
        temp = new Tile();
        test.addTile(new Coordinates(1, 5), temp);
        temp = new Tile();
        test.addTile(new Coordinates(2, 11), temp);
        temp = new Tile();
        test.addTile(new Coordinates(2, 10), temp);
        test.addPipe(new Coordinates(1, 0), 4);
        test.addPipe(new Coordinates(2, 11), 2);
        test.addPipe(new Coordinates(2, 11), 1);

        Assert.assertFalse(test.get(new Coordinates(2, 11)).hasPipe(1));
    }

    @Test
    void testGrowBamboo_GivenOneNonFertilisedTileWithNoNeighboursWithTheSameColorAndNoGardenerOnit_ThenBambooLevelIncrementsOnlyOnTheGivenTile() {
        Board test = new Board();
        Tile temp = new Tile(TileColor.GREEN, Improvement.IRRIGATED);
        test.addTile(new Coordinates(1, 0), temp);
        temp = new Tile(TileColor.GREEN, Improvement.IRRIGATED);
        test.addTile(new Coordinates(1, 5), temp);
        temp = new Tile(TileColor.GREEN, Improvement.IRRIGATED);
        test.addTile(new Coordinates(2, 11), temp);
        temp = new Tile(TileColor.PINK, Improvement.IRRIGATED);
        test.addTile(new Coordinates(2, 10), temp);

        test.growBamboo(new Coordinates(2, 10), false);

        Assert.assertTrue(test.get(new Coordinates(2, 10)).getBambooLevel() == 2);
        Assert.assertTrue(test.get(new Coordinates(2, 11)).getBambooLevel() == 1);
        Assert.assertTrue(test.get(new Coordinates(1, 5)).getBambooLevel() == 1);
    }

    @Test
    void testGrowBamboo_GivenOneNonFertilisedTileWithOneNeighbourWithTheSameColorAndNoGardenerOnit_ThenBambooLevelIncrementsOnlyOnTheGivenTile() {
        Board test = new Board();
        Tile temp = new Tile(TileColor.GREEN, Improvement.IRRIGATED);
        test.addTile(new Coordinates(1, 0), temp);
        temp = new Tile(TileColor.GREEN, Improvement.IRRIGATED);
        test.addTile(new Coordinates(1, 5), temp);
        temp = new Tile(TileColor.PINK, Improvement.IRRIGATED);
        test.addTile(new Coordinates(2, 11), temp);
        temp = new Tile(TileColor.PINK, Improvement.IRRIGATED);
        test.addTile(new Coordinates(2, 10), temp);

        test.growBamboo(new Coordinates(2, 10), false);

        Assert.assertTrue(test.get(new Coordinates(2, 10)).getBambooLevel() == 2);
        Assert.assertTrue(test.get(new Coordinates(2, 11)).getBambooLevel() == 1);
        Assert.assertTrue(test.get(new Coordinates(1, 5)).getBambooLevel() == 1);
    }

    @Test
    void testGrowBamboo_GivenOneWateredNonFertilisedTileWithNoNeighboursWithTheSameColorAndGardenerOnIt_ThenBambooLevelIncrementsOnlyOnTheGivenTile() {
        Board test = new Board();
        Tile temp = new Tile(TileColor.GREEN, Improvement.IRRIGATED);
        test.addTile(new Coordinates(1, 0), temp);
        temp = new Tile(TileColor.GREEN, Improvement.IRRIGATED);
        test.addTile(new Coordinates(1, 5), temp);
        temp = new Tile(TileColor.GREEN, Improvement.IRRIGATED);
        test.addTile(new Coordinates(2, 11), temp);
        temp = new Tile(TileColor.PINK, Improvement.IRRIGATED);
        test.addTile(new Coordinates(2, 10), temp);

        test.growBamboo(new Coordinates(2, 10), true);

        Assert.assertTrue(test.get(new Coordinates(2, 10)).getBambooLevel() == 2);
        Assert.assertTrue(test.get(new Coordinates(2, 11)).getBambooLevel() == 1);
        Assert.assertTrue(test.get(new Coordinates(1, 5)).getBambooLevel() == 1);
    }

    @Test
    void testGrowBamboo_GivenTwoWateredNeigbouringNonFertilisedTilesWithTheSameColorAndNoNeighboursWithTheSameColorAndGardenerOnOneOfThem_ThenBambooLevelIncrementsOnlyOnTheTwoGivenTiles() {
        Board test = new Board();
        Tile temp = new Tile(TileColor.GREEN, Improvement.IRRIGATED);
        test.addTile(new Coordinates(1, 0), temp);
        temp = new Tile(TileColor.GREEN, Improvement.IRRIGATED);
        test.addTile(new Coordinates(1, 5), temp);
        temp = new Tile(TileColor.PINK, Improvement.IRRIGATED);
        test.addTile(new Coordinates(2, 11), temp);
        temp = new Tile(TileColor.PINK, Improvement.IRRIGATED);
        test.addTile(new Coordinates(2, 10), temp);

        test.growBamboo(new Coordinates(2, 10), true);

        Assert.assertTrue(test.get(new Coordinates(2, 10)).getBambooLevel() == 2);
        Assert.assertTrue(test.get(new Coordinates(2, 11)).getBambooLevel() == 2);
        Assert.assertTrue(test.get(new Coordinates(1, 5)).getBambooLevel() == 1);
    }

    @Test
    void testGrowBamboo_GivenOneWateredAndOneNonWateredNonFertilisedTilesWithTheSameColorAndNoNeighboursWithTheSameColorAndGardenerOnTheWateredOne_ThenBambooLevelIncrementsOnlyOnTheGivenWateredTile() {
        Board test = new Board();
        Tile temp = new Tile(TileColor.GREEN, Improvement.IRRIGATED);
        test.addTile(new Coordinates(1, 0), temp);
        temp = new Tile(TileColor.GREEN, Improvement.IRRIGATED);
        test.addTile(new Coordinates(1, 5), temp);
        temp = new Tile(TileColor.PINK, Improvement.NONE);
        test.addTile(new Coordinates(2, 11), temp);
        temp = new Tile(TileColor.PINK, Improvement.IRRIGATED);
        test.addTile(new Coordinates(2, 10), temp);

        test.growBamboo(new Coordinates(2, 10), true);

        Assert.assertTrue(test.get(new Coordinates(2, 10)).getBambooLevel() == 2);
        Assert.assertTrue(test.get(new Coordinates(2, 11)).getBambooLevel() == 0);
        Assert.assertTrue(test.get(new Coordinates(1, 5)).getBambooLevel() == 1);
    }

    @Test
    void testGrowBamboo_GivenOneWateredAndOneNonWateredNonFertilisedTilesWithTheSameColorAndNoNeighboursWithTheSameColorAndGardenerOnTheNonWateredOne_ThenBambooLevelIncrementsOnlyOnTheGivenWateredTile() {
        Board test = new Board();
        Tile temp = new Tile(TileColor.GREEN, Improvement.IRRIGATED);
        test.addTile(new Coordinates(1, 0), temp);
        temp = new Tile(TileColor.GREEN, Improvement.IRRIGATED);
        test.addTile(new Coordinates(1, 5), temp);
        temp = new Tile(TileColor.PINK, Improvement.NONE);
        test.addTile(new Coordinates(2, 11), temp);
        temp = new Tile(TileColor.PINK, Improvement.IRRIGATED);
        test.addTile(new Coordinates(2, 10), temp);

        test.growBamboo(new Coordinates(2, 11), true);

        Assert.assertTrue(test.get(new Coordinates(2, 10)).getBambooLevel() == 2);
        Assert.assertTrue(test.get(new Coordinates(2, 11)).getBambooLevel() == 0);
        Assert.assertTrue(test.get(new Coordinates(1, 5)).getBambooLevel() == 1);
    }

    @Test
    void testGrowBamboo_GivenTwoWateredWithPipesNonFertilisedTilesWithTheSameColorAndNoNeighboursWithTheSameColorAndGardenerOnOneOfThem_ThenBambooLevelIncrementsOnlyOnBothOfThem() {
        Board test = new Board();
        Tile temp = new Tile(TileColor.GREEN, Improvement.NONE);
        test.addTile(new Coordinates(1, 0), temp);
        temp = new Tile(TileColor.GREEN, Improvement.NONE);
        test.addTile(new Coordinates(1, 5), temp);
        temp = new Tile(TileColor.PINK, Improvement.NONE);
        test.addTile(new Coordinates(2, 11), temp);
        temp = new Tile(TileColor.PINK, Improvement.NONE);
        test.addTile(new Coordinates(2, 10), temp);
        test.addPipe(new Coordinates(1, 0), 4);
        test.addPipe(new Coordinates(2, 11), 3);
        test.addPipe(new Coordinates(2, 10), 1);

        test.growBamboo(new Coordinates(2, 11), true);

        Assert.assertTrue(test.get(new Coordinates(2, 10)).getBambooLevel() == 2);
        Assert.assertTrue(test.get(new Coordinates(2, 11)).getBambooLevel() == 2);
        Assert.assertTrue(test.get(new Coordinates(1, 5)).getBambooLevel() == 1);
    }

    @Test
    void testGrowBamboo_GivenOneWateredWithPipesFertilisedTileWithNoNeighboursWithTheSameColorAndGardenerOnIt_ThenBambooLevelIncreasesByTwoOnTheGivenTileAndStaysTheSameOnTheOtherNeighbouringOnes() {
        Board test = new Board();
        Tile temp = new Tile(TileColor.GREEN, Improvement.NONE);
        test.addTile(new Coordinates(1, 0), temp);
        temp = new Tile(TileColor.GREEN, Improvement.NONE);
        test.addTile(new Coordinates(1, 5), temp);
        temp = new Tile(TileColor.GREEN, Improvement.NONE);
        test.addTile(new Coordinates(2, 11), temp);
        temp = new Tile(TileColor.PINK, Improvement.FERTILISED);
        test.addTile(new Coordinates(2, 10), temp);
        test.addPipe(new Coordinates(1, 0), 4);
        test.addPipe(new Coordinates(2, 11), 3);
        test.addPipe(new Coordinates(2, 10), 1);

        test.growBamboo(new Coordinates(2, 10), true);

        Assert.assertTrue(test.get(new Coordinates(2, 10)).getBambooLevel() == 4);
        Assert.assertTrue(test.get(new Coordinates(2, 11)).getBambooLevel() == 1);
        Assert.assertTrue(test.get(new Coordinates(1, 5)).getBambooLevel() == 1);
    }

    @Test
    void testGrowBamboo_GivenTwoWateredWithPipesFertilisedTilesWithTheSameColorAndNoNeighboursWithTheSameColorAndGardenerOnOneOfThem_ThenBambooLevelIncreasesByTwoOnBothOfThemAndStaysTheSameOnTheOtherNeighbouringOnes() {
        Board test = new Board();
        Tile temp = new Tile(TileColor.GREEN, Improvement.NONE);
        test.addTile(new Coordinates(1, 0), temp);
        temp = new Tile(TileColor.GREEN, Improvement.NONE);
        test.addTile(new Coordinates(1, 5), temp);
        temp = new Tile(TileColor.PINK, Improvement.FERTILISED);
        test.addTile(new Coordinates(2, 11), temp);
        temp = new Tile(TileColor.PINK, Improvement.FERTILISED);
        test.addTile(new Coordinates(2, 10), temp);
        test.addPipe(new Coordinates(1, 0), 4);
        test.addPipe(new Coordinates(2, 11), 3);
        test.addPipe(new Coordinates(2, 10), 1);

        test.growBamboo(new Coordinates(2, 10), true);

        Assert.assertTrue(test.get(new Coordinates(2, 10)).getBambooLevel() == 4);
        Assert.assertTrue(test.get(new Coordinates(2, 11)).getBambooLevel() == 4);
        Assert.assertTrue(test.get(new Coordinates(1, 5)).getBambooLevel() == 1);
    }

    @Test
    void testGrowBamboo_GivenTwoWateredWithPipesOneFertilisedAndOneNotTilesWithTheSameColorAndNoNeighboursWithTheSameColorAndGardenerOnOneOfThem_ThenBambooLevelIncreasesByTwoOnTheFertilisedOneAndByOneOnTheNonFertilisedOne() {
        Board test = new Board();
        Tile temp = new Tile(TileColor.GREEN, Improvement.NONE);
        test.addTile(new Coordinates(1, 0), temp);
        temp = new Tile(TileColor.GREEN, Improvement.NONE);
        test.addTile(new Coordinates(1, 5), temp);
        temp = new Tile(TileColor.PINK, Improvement.NONE);
        test.addTile(new Coordinates(2, 11), temp);
        temp = new Tile(TileColor.PINK, Improvement.FERTILISED);
        test.addTile(new Coordinates(2, 10), temp);
        test.addPipe(new Coordinates(1, 0), 4);
        test.addPipe(new Coordinates(2, 11), 3);
        test.addPipe(new Coordinates(2, 10), 1);

        test.growBamboo(new Coordinates(2, 10), true);

        Assert.assertTrue(test.get(new Coordinates(2, 10)).getBambooLevel() == 4);
        Assert.assertTrue(test.get(new Coordinates(2, 11)).getBambooLevel() == 2);
        Assert.assertTrue(test.get(new Coordinates(1, 5)).getBambooLevel() == 1);
    }

    @Test
    void testAddImprovement_GivenATileWithNoImprovementAndAValidImprovement_ThenTheTileHasTheGivenImprovement() {
        Board test = new Board();
        Tile temp = new Tile(TileColor.GREEN, Improvement.NONE);
        test.addTile(new Coordinates(1, 0), temp);
        temp = new Tile(TileColor.GREEN, Improvement.NONE);
        test.addTile(new Coordinates(1, 5), temp);
        temp = new Tile(TileColor.PINK, Improvement.NONE);
        test.addTile(new Coordinates(2, 11), temp);
        temp = new Tile(TileColor.PINK, Improvement.FERTILISED);
        test.addTile(new Coordinates(2, 10), temp);
        test.addPipe(new Coordinates(1, 0), 4);
        test.addPipe(new Coordinates(2, 11), 3);
        test.addPipe(new Coordinates(2, 10), 1);

        test.addImprovement(new Coordinates(2, 11), Improvement.FERTILISED);

        Assert.assertTrue(test.get(new Coordinates(2, 11)).getMod() == Improvement.FERTILISED);
    }

    @Test
    void testAddImprovement_GivenATileWithImprovementAndAValidImprovement_ThenTheTileHasTheOldImprovement() {
        Board test = new Board();
        Tile temp = new Tile(TileColor.GREEN, Improvement.NONE);
        test.addTile(new Coordinates(1, 0), temp);
        temp = new Tile(TileColor.GREEN, Improvement.NONE);
        test.addTile(new Coordinates(1, 5), temp);
        temp = new Tile(TileColor.PINK, Improvement.NONE);
        test.addTile(new Coordinates(2, 11), temp);
        temp = new Tile(TileColor.PINK, Improvement.FERTILISED);
        test.addTile(new Coordinates(2, 10), temp);
        test.addPipe(new Coordinates(1, 0), 4);
        test.addPipe(new Coordinates(2, 11), 3);
        test.addPipe(new Coordinates(2, 10), 1);

        test.addImprovement(new Coordinates(2, 10), Improvement.IRRIGATED);

        Assert.assertTrue(test.get(new Coordinates(2, 10)).getMod() == Improvement.FERTILISED);
    }

    @Test
    void testAddImprovement_GivenATileAndANonValidImprovement_ThenTheTileHasTheOldImprovement() {
        Board test = new Board();
        Tile temp = new Tile(TileColor.GREEN, Improvement.NONE);
        test.addTile(new Coordinates(1, 0), temp);
        temp = new Tile(TileColor.GREEN, Improvement.NONE);
        test.addTile(new Coordinates(1, 5), temp);
        temp = new Tile(TileColor.PINK, Improvement.NONE);
        test.addTile(new Coordinates(2, 11), temp);
        temp = new Tile(TileColor.PINK, Improvement.FERTILISED);
        test.addTile(new Coordinates(2, 10), temp);
        test.addPipe(new Coordinates(1, 0), 4);
        test.addPipe(new Coordinates(2, 11), 3);
        test.addPipe(new Coordinates(2, 10), 1);

        test.addImprovement(new Coordinates(2, 10), Improvement.NONE);
        test.addImprovement(new Coordinates(2, 11), Improvement.NONE);

        Assert.assertTrue(test.get(new Coordinates(2, 10)).getMod() == Improvement.FERTILISED);
        Assert.assertTrue(test.get(new Coordinates(2, 11)).getMod() == Improvement.NONE);
    }

    @Test
    void testAddImprovement_GivenANonWateredTileWithNoImprovementAndIrrigatingImprovement_ThenBambooLevelBecomesOneOnTheGivenTile() {
        Board test = new Board();
        Tile temp = new Tile(TileColor.GREEN, Improvement.NONE);
        test.addTile(new Coordinates(1, 0), temp);
        temp = new Tile(TileColor.GREEN, Improvement.NONE);
        test.addTile(new Coordinates(1, 5), temp);
        temp = new Tile(TileColor.PINK, Improvement.NONE);
        test.addTile(new Coordinates(2, 11), temp);
        temp = new Tile(TileColor.PINK, Improvement.NONE);
        test.addTile(new Coordinates(2, 10), temp);

        Assert.assertTrue(test.get(new Coordinates(2, 10)).getBambooLevel() == 0);

        test.addImprovement(new Coordinates(2, 10), Improvement.IRRIGATED);

        Assert.assertTrue(test.get(new Coordinates(2, 10)).getBambooLevel() == 1);
    }

    @Test
    void testAddImprovement_GivenAWateredTileWithNoImprovementAndIrrigatingImprovement_ThenBambooLevelStaysTheSame() {
        Board test = new Board();
        Tile temp = new Tile(TileColor.GREEN, Improvement.NONE);
        test.addTile(new Coordinates(1, 0), temp);
        temp = new Tile(TileColor.GREEN, Improvement.NONE);
        test.addTile(new Coordinates(1, 5), temp);
        temp = new Tile(TileColor.PINK, Improvement.NONE);
        test.addTile(new Coordinates(2, 11), temp);
        temp = new Tile(TileColor.PINK, Improvement.NONE);
        test.addTile(new Coordinates(2, 10), temp);
        test.addPipe(new Coordinates(1, 0), 4);
        test.addPipe(new Coordinates(2, 11), 3);
        test.addPipe(new Coordinates(2, 10), 1);

        Assert.assertTrue(test.get(new Coordinates(2, 10)).getBambooLevel() == 1);

        test.addImprovement(new Coordinates(2, 10), Improvement.IRRIGATED);

        Assert.assertTrue(test.get(new Coordinates(2, 10)).getBambooLevel() == 1);
    }

}
