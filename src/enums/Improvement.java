package enums;

public enum Improvement {
    NONE,
    IRRIGATED,
    PROTECTED,
    FERTILISED
}
