package board;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import enums.Improvement;
import enums.TileColor;

public class Board {
    private LinkedHashMap<Coordinates, Tile> board;
    private ArrayList<ArrayList<Coordinates>> tilesByColor;

    public Board() {
        this.board = new LinkedHashMap<Coordinates, Tile>();
        Tile first = new Tile();
        first.makePond();
        this.board.put(new Coordinates(0, 0), first);
        this.tilesByColor = new ArrayList<ArrayList<Coordinates>>();
        for (int i = 0; i < 3; i++) {
            this.tilesByColor.add(new ArrayList<Coordinates>());
        }
    }

    public Tile get(Coordinates key) {
        if (board.containsKey(key)) {
            return this.board.get(key);
        }
        return null;
    }

    public void addTile(Coordinates key, final Tile tile) {
        if (!key.isValid()) {
            System.out.println("Invalid coordinates!");
            return;
        } else {
            if (this.board.containsKey(key)) {
                System.out.println("Tile is already filled");
                return;
            } else {
                Tile newTile = new Tile(tile);
                if (this.board.size() == 1) {
                    if (key.getLevel() == 1) {
                        newTile.addPipe((key.getIndex() + 3));
                        this.board.put(key, newTile);
                        this.tilesByColor.get(tile.getColor().ordinal()).add(key);
                        System.out.println("Tile added at " + key + ".");
                    } else {
                        System.out.println("Invalid Coordinates!");
                        return;
                    }
                } else {
                    int counter = 0;
                    for (int i = 0; i < 6; i++) {
                        if (this.board.containsKey(key.getNeighbor(i))) {
                            counter++;
                        }
                    }
                    if (counter > 1) {
                        if (key.getLevel() == 1) {
                            newTile.addPipe((key.getIndex() + 3));
                        }
                        this.board.put(key, newTile);
                        this.tilesByColor.get(tile.getColor().ordinal()).add(key);

                        System.out.println("Tile added at" + key + ".");
                    } else {
                        System.out.println("A tile cannot be placed at " + key
                                + " because there are less than 2 neighboring tiles.");
                        return;
                    }
                }
                if (this.board.get(key).isWatered()) {
                    this.get(key).growBamboo();
                }
            }
        }
    }

    public void updateTilePipes(Coordinates key) {
        for (int i = 0; i < 6; i++) {
            Coordinates neighborKey = key.getNeighbor(i);
            if (this.board.containsKey(neighborKey)) {
                if ((this.board.get(neighborKey).hasPipe(i + 3))) {
                    this.get(key).addPipe(i);
                }
            }
        }
    }

    void print() {
        Iterator<Entry<Coordinates, Tile>> it = board.entrySet().iterator();
        while (it.hasNext()) {
            Entry<Coordinates, Tile> next = it.next();
            System.out.println(next.getKey() + " " + next.getValue());
        }
    }

    void addPipe(Coordinates key, int n) {
        Tile current = this.board.get(key);
        boolean irrigated = current.isWatered();
        if (current != null) {
            if (!current.hasPipe(n)) {
                Tile opposite = this.board.get(key.getNeighbor(n));
                if (opposite == null) {
                    System.out.println("The tile doesn't have a neighboring tile. A pipe can't be added there");
                } else {
                    boolean oppositeIrrigated = opposite.isWatered();
                    if (current.hasPipe(n + 1) || current.hasPipe(n + 5) || opposite.hasPipe(n + 2)
                            || opposite.hasPipe(n + 4)) {

                        current.addPipe(n);
                        opposite.addPipe(n + 3);
                        if (current.isWatered() != irrigated) {
                            current.growBamboo();
                        }
                        opposite.addPipe(n + 3);
                        if (opposite.isWatered() != oppositeIrrigated) {
                            opposite.growBamboo();
                        }
                        return;
                    }
                }
            } else {
                System.out.println("There's already a pipe there!");
            }
        }
        System.out.println("Cannot add pipe because there are no neighboring pipes to connect it to.");
    }

    public void growBamboo(Coordinates key, boolean gardener) {
        System.out.println("Growing on " + key);
        if (this.board.containsKey(key)) {
            if (gardener) {
                for (int i = 0; i < 6; i++) {
                    Coordinates neighbor = key.getNeighbor(i);
                    if (this.board.containsKey(neighbor)
                            && this.board.get(neighbor).getColor() == this.board
                                    .get(key).getColor()) {
                        this.board.get(neighbor).growBamboo();
                        System.out.println(
                                "Growing neighbor's " + neighbor + " bamboo");
                    }
                }
            }
            if (this.board.get(key).isWatered()) {
                System.out.println(key + " growing 1 bamboo");
                this.board.get(key).growBamboo();
                return;
            } else {
                System.out.println("Tile is not irrigated.");
                return;
            }
        }
        System.out.println("Tile doesn't exist.");
    }

    void addImprovement(Coordinates key, Improvement mod) {
        if (mod == Improvement.NONE) {
            System.out.println("You cant remove improvement");
            return;
        } else {
            if (this.board.containsKey(key)) {
                Tile tile = this.get(key);
                if (tile.getMod() == Improvement.NONE) {
                    if (mod == Improvement.IRRIGATED && !tile.isWatered()) {
                        tile.addMod(mod);
                        tile.growBamboo();
                    } else {
                        tile.addMod(mod);
                    }
                } else {
                    System.out.println("Tile already has improvement.");
                    return;
                }
            } else {
                System.out.println("No such tile.");
            }
        }
    }

    public ArrayList<Coordinates> getTilesByColor(TileColor color) {
        return this.tilesByColor.get(color.ordinal());
    }

    int getSize() {
        return this.board.size();
    }
}
