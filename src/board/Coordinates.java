package board;

public class Coordinates {
	int level;
	int index;
	Coordinates[] neighbors = null;

	public Coordinates() {
		this.level = 0;
		this.index = 0;
	}

	public Coordinates(int level, int index) {
		this.level = level;
		this.index = index;
	}

	public int getLevel() {
		return this.level;
	}

	public int getIndex() {
		return this.index;
	}

	public boolean isValid() {
		if ((this.index >= 0 && this.index < this.level * 6) && this.level >= 0) {
			return true;
		}
		return false;
	}

	public boolean isStraight() {
		if (this.level == 0 || this.level == 1) {
			return true;
		}
		return (this.index % this.level) == 0;
	}

	@Override
	public String toString() {
		return "[" + level + "," + index + "]";
	}

	public Coordinates getNeighbor(int n) {
		if (this.neighbors == null) {
			this.neighbors = getNeighbors();
		}
		n = n % 6;
		if (this.level == 0) {
			return this.neighbors[n];
		}
		return this.neighbors[(n + (6 - this.index / this.level)) % 6];
	}

	public Coordinates[] getNeighbors() {
		Coordinates[] result = new Coordinates[6];
		if (this.level == 0) {
			for (int i = 0; i < 6; i++) {
				result[i] = new Coordinates(1, i);
			}
			return result;
		}
		int n = this.level;
		int m = this.index;
		if (isStraight()) {
			result[3] = new Coordinates(n - 1, (m / n) * ((n - 1)));
			result[2] = new Coordinates(n, ((m + 1) % (n * 6)));
			result[4] = new Coordinates(n, (m + n * 6 - 1) % (n * 6));
			result[0] = new Coordinates(n + 1, (m / n) * ((n + 1)));
			result[1] = new Coordinates(n + 1, (((m / n) * (n + 1) + 1)) % ((n + 1) * 6));
			result[5] = new Coordinates(n + 1, ((((m / n) * (n + 1) + (n + 1) * 6 - 1))) % ((n + 1) * 6));
		} else {
			result[0] = new Coordinates((n + 1), ((((m - m % n) / n) * (n + 1) + m % n)) % ((n + 1) * 6));
			result[1] = new Coordinates((n + 1), ((((m - m % n) / n) * (n + 1) + m % n) + 1) % ((n + 1) * 6));
			result[5] = new Coordinates(n, (m + n * 6 - 1) % (n * 6));
			result[2] = new Coordinates(n, (m + 1) % (n * 6));
			result[4] = new Coordinates((n - 1), (((m - m % n) / n) * (n - 1) + (m % n - 1)) % ((n - 1) * 6));
			result[3] = new Coordinates((n - 1), (((m - m % n) / n) * (n - 1) + (m % n)) % ((n - 1) * 6));
		}
		return result;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + index;
		result = prime * result + level;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Coordinates other = (Coordinates) obj;
		if (index != other.index)
			return false;
		if (level != other.level)
			return false;
		return true;
	}
}
