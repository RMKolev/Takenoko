package quests;

public enum GardenShape {
    TRIANGLE,
    DIAMOND,
    CRESCENT,
    LINE
}
