package npc;

import board.Board;
import board.Coordinates;
import player.Player;

public class Gardener {
    Coordinates position;

    public Gardener() {
        this.position = new Coordinates();
    }
    
    void move(int direction, int steps, Board board, Player player) {
        Coordinates previousPosition = this.position;
        for (int i = 0; i < steps; i++) {
            if (board.get(this.position.getNeighbor(direction)) != null) {
                this.position = this.position.getNeighbor(direction);
            } else {
                this.position = previousPosition;
                return;
            }
        }
        board.growBamboo(this.position, true);
    }

}
